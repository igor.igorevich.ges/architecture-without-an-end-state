
# Yay, Merger!

We've acquired two competitors

1. Subscribr.com
  - Monorail application with a Postgres backend
2. subsee.ly
  - SPA front end calls API
  - Node.js API layer in front of
  - Customer service - python on Cassandra
    - Contains customers and subscription info
  - Product service - C# over Oracle
  - Customer and Product services are instanced per seller
  - Sellers have direct database access to their instances
  - Payments via Stripe

^ Subscribr hosted in AWS. $1M /yr to operate. Co-located team using agile dev. Using own payment system (connected to banks)
Subsee.ly in Rackspace VMs. $5M/yr operate. Distributed teams, cowboy devops.
If merging or shutting down systems, show the migration path.
