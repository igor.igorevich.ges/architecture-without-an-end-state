
# David Parnas

"On the Criteria To Be Used in Decomposing Systems into Modules", CACM, 1972

---

# KWIC Index

"Permuted Index"

Input: ordered set of lines, made up of ordered words, made up of ordered characters.
Output: Listing of all "circular shifts" of all lines, in alphabetic order

---

# Modularization I

1. Input: read data lines, store them in core. Characters packed 4 per word. EOW special character.
2. Circular shift: prepare index; addr of first char of each shift, original index of line in array from module 1.
3. Alphabetizing: Take arrays from 1 & 2, produce same shape array as 2, but in alpha order.
4. Output: Using arrays from 1 & 3, format output
5. Control: sequence operations of 1 - 4, allocate memory, print errors.

^Ask someone to sketch this architecture

---

# Modularization II

1. Line storage: functional interface; functions to set/get char in line, get words in a line, delete words or lines
2. Input: read input call line storage.
3. Circular shifter: present same interface as 1, but appear to have all shifts of all lines
4. Alphabetizer: sort function ("ALPH") and access function ("iTH")
5. Output: Print output by calling "iTH" on the alphabetizer
6. Control: similar to first modularization

^Ask someone to sketch this architecture

---

# Impact of Changes?

1. Alter the input format.
1. Use offline storage for large jobs.
1. Stop using packed characters, write character per word.
1. Write index for circular shifts to offline storage instead of core.
1. Support Unicode

---

# [fit] Hiding
# [fit] Information, Decisions, Concepts

---

# Bounded Contexts and Anticorruption Layers

![inline](http://martinfowler.com/bliki/images/boundedContext/sketch.png)

http://martinfowler.com/bliki/BoundedContext.html

---

# Implications of Bounded Contexts

- Data duplication is OK
- Similar entity's lifecycles may differ
- Direct access to interior is prohibited
